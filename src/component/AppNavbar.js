import { Navbar, Container, Nav, NavDropdown, Image } from "react-bootstrap"
import { Link } from "react-router-dom"
import BrandLogo from '../assets/img/book-logo1.png'
import Login from "../pages/Login"
const AppNavbar = () => {
    return (
       <>
         <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Link className='nav-link' to={'/'}>
                  <Image className='brand-logo' src={BrandLogo} />
                </Link>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link className='nav-link' >Home</Nav.Link>
                        <Nav.Link href="#pricing">Pricing</Nav.Link>
                        <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">
                                Another action
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">
                                Separated link
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Nav>
                        <Link to={'/login'}>Login</Link>
                        <Nav.Link eventKey={2} href="#memes">
                            Dank memes
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>       
       </>
        
    )
}
export default AppNavbar