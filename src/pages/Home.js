import { Col, Container, Row } from "react-bootstrap"
import AppFeature from "../component/AppFeature"
import AppFooter from "../component/AppFooter"
import AppNavbar from "../component/AppNavbar"
import AppStarter from "../component/AppStarter"
import BookCard from "../component/BookCard"
import Book1Img from "../assets/img/book1.jpg"
import Book2Img from "../assets/img/book1.jpg"
import Book3Img from "../assets/img/book1.jpg"
import Book4Img from "../assets/img/book2.png"
import Book5Img from "../assets/img/book2.png"
import Book6Img from "../assets/img/book2.png"

const Home = () => {
    return (
        <>

         <AppNavbar />
         <AppFeature />
         <AppStarter />

         <Container>

            <h1 className="my-4">Find your fav book here</h1>

            <Row className="g-3">
                <Col sm={12} md={4} lg= {4} >
                   <BookCard book={Book1Img} />
                </Col>
                <Col sm={12} md={4} lg= {4} >
                   <BookCard book={Book2Img} />
                </Col>
                <Col sm={12} md={4} lg= {4} >
                   <BookCard book={Book3Img} />
                </Col>
                <Col sm={12} md={4} lg= {4} >
                   <BookCard book={Book4Img}/>
                </Col>
                <Col sm={12} md={4} lg= {4} >
                   <BookCard book={Book5Img} />
                </Col>
                <Col sm={12} md={4} lg= {4} >
                   <BookCard book={Book6Img} />
                </Col>
            </Row>
         </Container>

         <AppFooter />
        </>
    )
}
export default Home